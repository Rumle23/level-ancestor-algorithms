use std::alloc::{GlobalAlloc, Layout};
use std::sync::atomic::{AtomicUsize, Ordering};

pub struct TrackingAllocator<A: GlobalAlloc> {
    allocator: A,
    current_memory_usage: AtomicUsize,
    max_memory_usage: AtomicUsize,
}

unsafe impl<A: GlobalAlloc> GlobalAlloc for TrackingAllocator<A> {
    unsafe fn alloc(&self, l: Layout) -> *mut u8 {
        self.current_memory_usage
            .fetch_add(l.size(), Ordering::SeqCst);
        self.max_memory_usage.fetch_max(
            self.current_memory_usage.load(Ordering::SeqCst),
            Ordering::SeqCst,
        );
        self.allocator.alloc(l)
    }

    unsafe fn dealloc(&self, ptr: *mut u8, l: Layout) {
        self.allocator.dealloc(ptr, l);
        self.current_memory_usage
            .fetch_sub(l.size(), Ordering::SeqCst);
    }
}

impl<A: GlobalAlloc> TrackingAllocator<A> {
    pub const fn new(a: A) -> Self {
        TrackingAllocator {
            allocator: a,
            current_memory_usage: AtomicUsize::new(0),
            max_memory_usage: AtomicUsize::new(0),
        }
    }

    pub fn reset(&self) {
        self.current_memory_usage.store(0, Ordering::SeqCst);
        self.max_memory_usage.store(0, Ordering::SeqCst);
    }

    pub fn get_current(&self) -> usize {
        self.current_memory_usage.load(Ordering::SeqCst)
    }

    pub fn get_max(&self) -> usize {
        self.max_memory_usage.load(Ordering::SeqCst)
    }
}
