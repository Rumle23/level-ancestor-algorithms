mod tracking_allocator;

use std::alloc::System;
use std::fmt;
use std::ops::{Add, AddAssign, Div, Sub};
use std::time::{Duration, Instant};
use tracking_allocator::TrackingAllocator;

#[global_allocator]
pub static GLOBAL: TrackingAllocator<System> = TrackingAllocator::new(System);

#[derive(Debug, Clone, Copy)]
pub struct BenchResult {
    time: Duration,
    max_memory: usize,
    current_memory: usize,
}

impl BenchResult {
    pub fn new(time: Duration, max_memory: usize, current_memory: usize) -> Self {
        BenchResult {
            time,
            max_memory,
            current_memory,
        }
    }
    pub fn time(&self) -> &Duration {
        &self.time
    }

    pub fn max_memory(&self) -> &usize {
        &self.max_memory
    }

    pub fn current_memory(&self) -> &usize {
        &self.current_memory
    }
}

impl fmt::Display for BenchResult {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(
            formatter,
            "{{ time: {} ms, max_mem: {} MB, cur_mem: {} MB}}",
            self.time.as_millis(),
            self.max_memory() / (1024 * 1024),
            self.current_memory() / (1024 * 1024)
        )
    }
}

impl Add for BenchResult {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        BenchResult {
            time: self.time + rhs.time,
            max_memory: self.max_memory + rhs.max_memory,
            current_memory: self.current_memory + rhs.current_memory,
        }
    }
}

impl Sub for BenchResult {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self::Output {
        BenchResult {
            time: self.time - rhs.time,
            max_memory: self.max_memory - rhs.max_memory,
            current_memory: self.current_memory - rhs.current_memory,
        }
    }
}

impl Div<u32> for BenchResult {
    type Output = Self;
    fn div(self, rhs: u32) -> Self::Output {
        BenchResult {
            time: self.time / rhs,
            max_memory: self.max_memory / (rhs as usize),
            current_memory: self.current_memory / (rhs as usize),
        }
    }
}

impl AddAssign for BenchResult {
    fn add_assign(&mut self, rhs: Self) {
        self.time += rhs.time;
        self.max_memory += rhs.max_memory;
        self.current_memory += rhs.current_memory;
    }
}

pub fn bench<R, F: FnOnce() -> R>(func: F) -> (BenchResult, R) {
    GLOBAL.reset();
    let start_time = Instant::now();
    let ret = func();
    let complete_time = Instant::now();
    (
        BenchResult {
            time: complete_time.duration_since(start_time),
            max_memory: GLOBAL.get_max(),
            current_memory: GLOBAL.get_current(),
        },
        ret,
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::level_ancestor::Preprocessor;
    use crate::tree_generator::balanced_tree::generate_balanced_tree;

    #[test]
    fn addition() {
        let mut lhs = BenchResult::new(Duration::new(0, 0), 0, 0);
        let rhs = BenchResult::new(Duration::new(1, 0), 0, 0);
        lhs += rhs;
        assert_eq!(rhs.time.as_secs(), 1);
        assert_eq!(lhs.time.as_secs(), 1);
        assert_eq!((rhs + rhs).time.as_secs(), 2);
    }
}
