use super::jump::JumpVec;
use super::ladder::LadderManager;
use super::preprocessor::Preprocessor;
use super::{LevelAncestor, LevelAncestorBuilder};
use crate::graph::{NodeId, NodeIdVec, Tree};
use crate::search::{AncestorSearch, IteratorAdapter, TreeDfs, TreeHeightSearch};
use std::collections::hash_map::Entry;
use std::collections::HashMap;

fn get_bit(input: usize, bit_index: u8) -> bool {
    input & (1 << bit_index) != 0
}

pub struct DFSTreeLA {
    ancestor_map: Vec<Vec<usize>>,
}

impl DFSTreeLA {
    /// Maintain a stack of the current ancestor_list.
    /// When following a down edge, push the current dfs number to the stack and clone the ancestor_list as the node's ancestor_list.
    /// When visiting a up edge, pop from the stack
    pub fn build(dfs_bit_vec: usize, longest_prefix_length: u8) -> DFSTreeLA {
        let mut next_dfs_num = 1;
        // The root always has a table consisting of itself.
        let mut current_ancestor_table = vec![0];
        let mut ancestor_map = vec![current_ancestor_table.clone()];
        for bit in (0..longest_prefix_length).map(|x| get_bit(dfs_bit_vec, x)) {
            match bit {
                true => {
                    current_ancestor_table.pop();
                }
                false => {
                    current_ancestor_table.push(next_dfs_num);
                    next_dfs_num += 1;
                    ancestor_map.push(current_ancestor_table.iter().cloned().rev().collect());
                }
            }
        }
        DFSTreeLA { ancestor_map }
    }

    pub fn query(&self, dfs_num: usize, ancestor_num: usize) -> Option<usize> {
        self.ancestor_map
            .get(dfs_num)
            .and_then(|ancestor_list| ancestor_list.get(ancestor_num).cloned())
    }
}

pub struct MicroManager {
    bitvec_to_index: HashMap<usize, usize>,
    tree_vec: Vec<DFSTreeLA>,
}

impl MicroManager {
    pub fn build(number_of_nodes: u8, lazy: bool) -> MicroManager {
        let mut micro_manager = MicroManager {
            bitvec_to_index: HashMap::new(),
            tree_vec: Vec::new(),
        };

        if lazy {
            return micro_manager;
        }

        let mut bitvec = 0;

        while !get_bit(bitvec, number_of_nodes * 2) {
            micro_manager.add_tree(bitvec);
            bitvec += 1;
        }
        micro_manager
    }

    pub fn get_tree_from_index(&self, index: usize) -> Option<&DFSTreeLA> {
        self.tree_vec.get(index)
    }

    pub fn get_or_create_tree_index(&mut self, dfs_bit_vec: usize) -> usize {
        self.add_tree(dfs_bit_vec)
    }

    pub fn get_tree_index(&self, dfs_bit_vec: usize) -> Option<usize> {
        let longest_prefix_length = MicroManager::verify_dfs_bit_vec(dfs_bit_vec);
        let longest_prefix =
            MicroManager::unset_bits_outside_prefix(dfs_bit_vec, longest_prefix_length);
        self.bitvec_to_index.get(&longest_prefix).cloned()
    }

    pub fn get_tree(&self, dfs_bit_vec: usize) -> Option<&DFSTreeLA> {
        self.get_tree_index(dfs_bit_vec)
            .and_then(|i| self.tree_vec.get(i))
    }

    fn add_tree(&mut self, dfs_bit_vec: usize) -> usize {
        let longest_prefix_length = MicroManager::verify_dfs_bit_vec(dfs_bit_vec);
        let longest_prefix =
            MicroManager::unset_bits_outside_prefix(dfs_bit_vec, longest_prefix_length);
        match self.bitvec_to_index.entry(longest_prefix) {
            Entry::Occupied(e) => *e.get(),
            Entry::Vacant(v) => {
                let i = self.tree_vec.len();
                v.insert(self.tree_vec.len());
                self.tree_vec
                    .push(DFSTreeLA::build(dfs_bit_vec, longest_prefix_length));
                i
            }
        }
    }

    fn unset_bits_outside_prefix(input: usize, prefix: u8) -> usize {
        let mut result = input;
        for i in (prefix as usize)..std::mem::size_of::<usize>() {
            result &= !(1 << i);
        }
        result
    }

    fn verify_dfs_bit_vec(dfs_bit_vec: usize) -> u8 {
        let mut balance = 0;
        let mut longest_correct_prefix = 0;
        let mut number_of_bits = 0;
        for bit in (0..std::mem::size_of::<usize>() * 8).map(|x| get_bit(dfs_bit_vec, x as u8)) {
            number_of_bits += 1;
            balance += match bit {
                true => 1,
                false => -1,
            };
            if balance == 0 {
                longest_correct_prefix = number_of_bits;
            } else if balance > 0 {
                return 0;
            }
        }
        longest_correct_prefix
    }
}

#[derive(Clone, Copy, Debug)]
pub enum NodeType {
    MicroNode(NodeId, usize),
    MacroNode(NodeId),
    JumpNode(usize),
}

fn create_jump_table(ladder_manager: &LadderManager, node: NodeId, _depth: usize) -> JumpVec {
    let mut table = JumpVec::new();
    table.add_jump(node);
    let mut jump = 1;
    if let Some(mut last_ancestor) = ladder_manager.get_ancestor(&node, 1).cloned() {
        table.add_jump(last_ancestor);
        while let Some(ancestor) = ladder_manager.get_ancestor(&last_ancestor, jump) {
            table.add_jump(*ancestor);
            last_ancestor = *ancestor;
            jump *= 2;
        }
    }
    table
}

fn dfs_bit_vec(root: NodeId, t: &Tree, pre: &Preprocessor) -> usize {
    let root_depth = pre.depth(&root).unwrap();
    let search = TreeDfs::new(root, t);
    let mut bv = 0;
    let mut index = 0;
    let mut last_depth: Option<usize> = None;
    // Skip root
    for node in IteratorAdapter::new(search, t).skip(1) {
        // If next node is deeper, then it is only a down edge
        // Else there are up edges equal to the difference in depth + 1 and a single down edge
        let depth = pre.depth(&node).unwrap();
        if let Some(ld) = last_depth {
            if ld >= *depth {
                for _ in 0..ld - depth + 1 {
                    bv |= 1 << index;
                    index += 1;
                }
            }
        }
        last_depth = Some(*depth);
        index += 1;
    }
    if let Some(depth) = last_depth {
        for _ in 0..depth - root_depth {
            bv |= 1 << index;
            index += 1;
        }
    }
    bv
}

pub struct MacroMicroLABuilder {
    lazy: bool,
    descendants_threshold_func: Box<dyn Fn(usize) -> u8>,
}

impl MacroMicroLABuilder {
    pub fn lazy<'a>(&'a mut self, lazy: bool) -> &'a Self {
        self.lazy = lazy;
        self
    }

    pub fn descendants_threshold_func<'a>(
        &'a mut self,
        descendants_threshold_func: Box<dyn Fn(usize) -> u8>,
    ) -> &'a Self {
        self.descendants_threshold_func = descendants_threshold_func;
        self
    }
}

impl LevelAncestorBuilder<MacroMicroLA> for MacroMicroLABuilder {
    fn new() -> Self {
        MacroMicroLABuilder {
            lazy: false,
            descendants_threshold_func: Box::new(|x| ((x as f64).log2() / 4.0) as u8),
        }
    }

    fn build(self, tree: &Tree, preprocessor: Preprocessor) -> MacroMicroLA {
        MacroMicroLA::build(
            tree,
            preprocessor,
            self.descendants_threshold_func,
            self.lazy,
        )
    }
}

pub struct MacroMicroLA {
    ladder_manager: LadderManager,
    node_type: NodeIdVec<NodeType>,
    jump_map: Vec<JumpVec>,
    micro_manager: MicroManager,
    preprocessor: Preprocessor,
}

impl MacroMicroLA {
    pub fn build<F>(
        g: &Tree,
        preprocessor: Preprocessor,
        descendants_threshold_func: F,
        lazy_micromanager: bool,
    ) -> MacroMicroLA
    where
        F: Fn(usize) -> u8,
    {
        let descendants_threshold = descendants_threshold_func(g.nodes().len());
        let ladder_manager = LadderManager::build(g, 2);
        let mut micro_manager = MicroManager::build(descendants_threshold, lazy_micromanager);
        let mut jump_nodes = Vec::new();
        let mut jump_map = Vec::new();
        let mut node_types = NodeIdVec(vec![None; g.nodes().len()]);
        // Find all jump nodes that can satisfy descendant constraint.
        {
            let mut search = TreeHeightSearch::new(g);
            let mut descendants = NodeIdVec(vec![0; g.nodes().len()]);
            while let Some((node_id, _)) = search.next(g) {
                let node = &g[node_id];
                if let Some(node_type) = node_types[*node.id()] {
                    if let Some(parent) = node.parent() {
                        node_types[*parent] = Some(node_type);
                    }
                } else if descendants[node_id] >= descendants_threshold {
                    node_types[node_id] = Some(NodeType::JumpNode(jump_nodes.len()));
                    jump_nodes.push(node_id);
                    let jump_table = create_jump_table(
                        &ladder_manager,
                        node_id,
                        *preprocessor.depth(&node_id).unwrap(),
                    );
                    jump_map.push(jump_table);
                    if let Some(parent) = node.parent() {
                        node_types[*parent] = Some(NodeType::MacroNode(node_id));
                    }
                } else if let Some(parent) = node.parent() {
                    descendants[*parent] += descendants[node_id] + 1;
                }
            }
        }

        for node in &jump_nodes {
            for micro_root in g[*node].children() {
                let bv = dfs_bit_vec(*micro_root, g, &preprocessor);
                let value =
                    NodeType::MicroNode(*micro_root, micro_manager.get_or_create_tree_index(bv));
                for node in IteratorAdapter::new(TreeDfs::new(*micro_root, g), g) {
                    node_types[node] = Some(value);
                }
            }
        }

        // Find remaining jump nodes
        for leaf in g.nodes().iter().filter(|node| node.children().is_empty()) {
            let leaf_id = *leaf.id();
            if node_types[leaf_id].is_none() {
                node_types[leaf_id] = Some(NodeType::JumpNode(jump_nodes.len()));
                jump_nodes.push(leaf_id);
                let jump_table = create_jump_table(
                    &ladder_manager,
                    leaf_id,
                    *preprocessor.depth(&leaf_id).unwrap(),
                );
                jump_map.push(jump_table);
                // Traverse ancestors until macro node is hit
                let search = AncestorSearch::new(leaf_id, g);
                for ancestor in IteratorAdapter::new(search, g) {
                    if node_types[ancestor].is_none() {
                        node_types[ancestor] = Some(NodeType::MacroNode(leaf_id));
                    } else {
                        break;
                    }
                }
            }
        }

        MacroMicroLA {
            ladder_manager,
            node_type: NodeIdVec(node_types.iter().map(|x| x.unwrap()).collect()),
            jump_map,
            micro_manager,
            preprocessor,
        }
    }

    fn jump_then_ladder(&self, node: &NodeId, number: usize) -> Option<&NodeId> {
        let jump_node = self.jump_node(node)?;
        let node_depth = self.node_depth(node)?;
        let jump_depth = self.node_depth(&jump_node)?;
        let jump_vec = self.jump_vec(&jump_node)?;
        jump_vec
            .jump(number + jump_depth - node_depth)
            .and_then(|(next_node, remainder)| {
                self.ladder_manager.get_ancestor(next_node, remainder)
            })
    }

    fn micro_ancestor(&self, node: &NodeId, number: usize) -> Option<&NodeId> {
        let node_depth = self.node_depth(node)?;
        if let Some(NodeType::MicroNode(root, micro_index)) = self.node_type.get(*node) {
            let root_depth = self.node_depth(root)?;
            if root_depth + number > node_depth {
                return self.jump_node(root).and_then(|jn| {
                    self.jump_then_ladder(&jn, number + root_depth - node_depth - 1)
                });
            } else {
                let node_dfs_number = self.preprocessor.dfs_number(&node)?;
                let root_dfs_number = self.preprocessor.dfs_number(root)?;
                let dfs_tree_la = self.micro_manager.get_tree_from_index(*micro_index)?;
                return dfs_tree_la
                    .query(node_dfs_number - root_dfs_number, number)
                    .and_then(|dfs_number| {
                        self.preprocessor.node_id(dfs_number + root_dfs_number)
                    });
            }
        }
        None
    }

    pub fn get_ancestor(&self, node: &NodeId, number: usize) -> Option<&NodeId> {
        if number == 0 {
            return self.ladder_manager.get_ancestor(&node, 0);
        }
        match self.node_type.get(*node) {
            Some(NodeType::MicroNode(_root, _micro_index)) => self.micro_ancestor(&node, number),
            Some(NodeType::MacroNode(_node_jump)) => self.jump_then_ladder(&node, number),
            Some(NodeType::JumpNode(_)) => self.jump_then_ladder(&node, number),
            None => None,
        }
    }

    pub fn jump_node(&self, node: &NodeId) -> Option<NodeId> {
        match self.node_type.get(*node) {
            Some(NodeType::MicroNode(_, _)) => self.ladder_manager.get_ancestor(node, 1).cloned(),
            Some(NodeType::MacroNode(node_jump)) => Some(*node_jump),
            Some(NodeType::JumpNode(_)) => Some(node).cloned(),
            None => None,
        }
    }

    fn jump_vec(&self, node: &NodeId) -> Option<&JumpVec> {
        match self.node_type.get(*node) {
            Some(NodeType::JumpNode(i)) => self.jump_map.get(*i),
            _ => None,
        }
    }

    pub fn node_depth(&self, node: &NodeId) -> Option<usize> {
        self.preprocessor.depth(node).cloned()
    }
}

impl LevelAncestor for MacroMicroLA {
    fn query(&self, node: &NodeId, depth: usize) -> Option<&NodeId> {
        match self
            .preprocessor
            .depth(node)
            .map(|node_depth| node_depth.checked_sub(depth))
            .flatten()
        {
            Some(relative_depth) => self.get_ancestor(node, relative_depth),
            None => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::level_ancestor::preprocessor::Preprocessor;
    use crate::level_ancestor::table::TableLA;
    use crate::tree_generator::balanced_tree::generate_balanced_tree;

    fn build_la(depth: usize, branching: usize) -> (Tree, MacroMicroLA) {
        let t = generate_balanced_tree(depth, branching);
        let preprocessor = Preprocessor::build(&t);
        let la = MacroMicroLABuilder::new().build(&t, preprocessor);
        (t, la)
    }

    #[test]
    fn micro_tree_on_empty_bitvec() {
        let dfs_tree_la = DFSTreeLA::build(0, 0);
        assert_eq!(dfs_tree_la.ancestor_map[0], vec![0]);
        assert_eq!(dfs_tree_la.ancestor_map.len(), 1);
    }

    #[test]
    fn micro_tree_small_tree() {
        let mut bv = 0;
        //    0
        //   / \
        //  1   4
        // / \
        //2   3
        bv |= 1 << 2;
        bv |= 1 << 4;
        bv |= 1 << 5;
        bv |= 1 << 7;
        let dfs_tree_la = DFSTreeLA::build(bv, 8);
        assert_eq!(dfs_tree_la.ancestor_map[2][0], 2);
        assert_eq!(dfs_tree_la.ancestor_map[2][1], 1);
        assert_eq!(dfs_tree_la.ancestor_map[2][2], 0);
        assert_eq!(dfs_tree_la.ancestor_map[3][0], 3);
        assert_eq!(dfs_tree_la.ancestor_map[3][1], 1);
        assert_eq!(dfs_tree_la.ancestor_map[3][2], 0);
        assert_eq!(dfs_tree_la.ancestor_map[4][0], 4);
        assert_eq!(dfs_tree_la.ancestor_map[4][1], 0);
    }

    #[test]
    fn micro_manager_generates_trees() {
        let mut bv = 0;
        //    0
        //   / \
        //  1   4
        // / \
        //2   3
        bv |= 1 << 2;
        bv |= 1 << 4;
        bv |= 1 << 5;
        bv |= 1 << 7;
        let micro_manager = MicroManager::build(5, false);
        assert_ne!(micro_manager.tree_vec.len(), 0);
        let dfs_tree_la = micro_manager.get_tree(bv).unwrap();
        assert_eq!(dfs_tree_la.ancestor_map[2][0], 2);
        assert_eq!(dfs_tree_la.ancestor_map[2][1], 1);
        assert_eq!(dfs_tree_la.ancestor_map[2][2], 0);
        assert_eq!(dfs_tree_la.ancestor_map[3][0], 3);
        assert_eq!(dfs_tree_la.ancestor_map[3][1], 1);
        assert_eq!(dfs_tree_la.ancestor_map[3][2], 0);
        assert_eq!(dfs_tree_la.ancestor_map[4][0], 4);
        assert_eq!(dfs_tree_la.ancestor_map[4][1], 0);
    }

    #[test]
    fn dfs_bitvec_calculates_correct() {
        let mut t = Tree::new();
        let r = t.create_root().unwrap();
        let n1 = t.add_child(r).unwrap();
        t.add_child(n1);
        t.add_child(n1);
        t.add_child(r);
        //    0
        //   / \
        //  1   4
        // / \
        //2   3
        let mut bv = 0;
        bv |= 1 << 2;
        bv |= 1 << 4;
        bv |= 1 << 5;
        bv |= 1 << 7;
        let pre = Preprocessor::build(&t);
        let bv1 = dfs_bit_vec(r, &t, &pre);
        assert_eq!(bv1, bv);
    }

    #[test]
    fn get_ancestor_can_return_own_node() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(la.get_ancestor(node.id(), 0), Some(node.id()));
        }
    }

    #[test]
    fn get_ancestor_can_return_parent() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(la.get_ancestor(node.id(), 1).cloned(), *node.parent());
        }
    }

    #[test]
    fn query_at_same_depth_returns_node() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(
                la.query(node.id(), *la.preprocessor.depth(node.id()).unwrap()),
                Some(node.id())
            );
        }
    }

    #[test]
    fn root_is_depth_zero() {
        let (t, la) = build_la(3, 2);
        for node in t.nodes() {
            assert_eq!(la.query(node.id(), 0), Some(&t.root().unwrap()));
        }
    }

    #[test]
    fn deeper_depth_fails() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(
                la.query(node.id(), la.preprocessor.depth(node.id()).unwrap() + 1),
                None
            );
        }
    }

    #[test]
    fn query_behaves_like_table_query() {
        let (t, la) = build_la(5, 4);
        let table = TableLA::build(&t, Preprocessor::build(&t));

        for node in t.nodes() {
            for depth in 0..(la.preprocessor.depth(node.id()).unwrap() + 1) {
                assert_eq!(table.query(node.id(), depth), la.query(node.id(), depth));
            }
        }
    }

    #[test]
    fn query_behaves_like_jumpladder_query_big() {
        use crate::level_ancestor::jumpladder::JumpLadderLA;
        let (t, la) = build_la(7, 4);
        let la2 = JumpLadderLA::build(&t, Preprocessor::build(&t));

        for node in t.nodes() {
            for depth in 0..(la.preprocessor.depth(node.id()).unwrap() + 1) {
                assert_eq!(la2.query(node.id(), depth), la.query(node.id(), depth));
            }
        }
    }
}
