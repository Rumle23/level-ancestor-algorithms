use crate::graph::{NodeId, NodeIdVec, Tree};
use crate::search::{TreeDfs, TreeIterator};

#[allow(dead_code)]
pub struct Preprocessor {
    depth: NodeIdVec<usize>,
    dfs_number: NodeIdVec<usize>,
    dfs_number_to_id: Vec<NodeId>,
}

#[allow(dead_code)]
impl Preprocessor {
    pub fn build(t: &Tree) -> Preprocessor {
        let mut depth = NodeIdVec(vec![0; t.nodes().len()]);
        let mut dfs_number = NodeIdVec(vec![0; t.nodes().len()]);
        let mut dfs_current_number = 1;
        let mut dfs_number_to_id = vec![NodeId::new(0); t.nodes().len()];
        if let Some(root) = t.root() {
            let mut dfs = TreeDfs::new(t.root().unwrap(), &t);
            dfs_number_to_id[0] = *root;
            // Skip root node
            dfs.next(&t);

            while let Some(node) = dfs.next(&t) {
                depth[node] = depth[t[node].parent().unwrap()] + 1;
                dfs_number[node] = dfs_current_number;
                dfs_number_to_id[dfs_current_number] = node;
                dfs_current_number += 1;
            }
        }

        Preprocessor {
            depth,
            dfs_number,
            dfs_number_to_id,
        }
    }

    pub fn depth(&self, id: &NodeId) -> Option<&usize> {
        self.depth.get(*id)
    }

    pub fn dfs_number(&self, id: &NodeId) -> Option<&usize> {
        self.dfs_number.get(*id)
    }

    pub fn node_id(&self, index: usize) -> Option<&NodeId> {
        self.dfs_number_to_id.get(index)
    }
}
