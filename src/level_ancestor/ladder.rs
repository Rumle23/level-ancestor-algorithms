use super::preprocessor::Preprocessor;
use super::{LevelAncestor, LevelAncestorBuilder};
use crate::graph::{Node, NodeId, NodeIdVec, Tree};
use crate::search::{AncestorSearch, IteratorAdapter, TreeHeightSearch};

pub struct LadderManager {
    ladders: Vec<Vec<NodeId>>,
    node_height: NodeIdVec<usize>,
    node_ladder_index: NodeIdVec<usize>,
}

impl LadderManager {
    pub fn build(tree: &Tree, extend_multiplyer: usize) -> LadderManager {
        let leaves: Vec<&Node> = tree
            .nodes()
            .iter()
            .filter(|node| node.children().is_empty())
            .collect();
        let mut manager = LadderManager {
            ladders: vec![Vec::new(); leaves.len()],
            node_height: NodeIdVec(vec![0; tree.nodes().len()]),
            node_ladder_index: NodeIdVec(vec![0; tree.nodes().len()]),
        };
        for (next_ladder, leave) in leaves.into_iter().enumerate() {
            manager.node_ladder_index[*leave.id()] = next_ladder;
        }
        let mut ths = TreeHeightSearch::new(tree);
        while let Some((node, height)) = ths.next(tree) {
            if let Some(max_child) = tree[node].children().iter().max_by(|child1, child2| {
                manager.node_height[**child1].cmp(&manager.node_height[**child2])
            }) {
                manager.node_ladder_index[node] = manager.node_ladder_index[*max_child];
            }
            manager.node_height[node] = height;
            manager.node_ladder_mut(&node).unwrap().push(node);
        }

        for ladder in &mut manager.ladders {
            let search = AncestorSearch::new(*ladder.last().unwrap(), tree);
            ladder
                .extend(IteratorAdapter::new(search, tree).take(ladder.len() * extend_multiplyer));
        }
        manager
    }
    fn node_ladder_mut(&mut self, node: &NodeId) -> Option<&mut Vec<NodeId>> {
        self.node_ladder_index
            .get(*node)
            .cloned()
            .and_then(move |ladder_index| self.ladders.get_mut(ladder_index))
    }

    pub fn node_ladder(&self, node: &NodeId) -> Option<&Vec<NodeId>> {
        self.node_ladder_index
            .get(*node)
            .map(|ladder_index| self.ladders.get(*ladder_index))
            .flatten()
    }

    pub fn follow_ladder(&self, node: &NodeId, delta_depth: usize) -> Option<(&NodeId, usize)> {
        let ladder = self.node_ladder(node)?;
        let index = self
            .node_height
            .get(*node)
            .map(|height| height + delta_depth)?;

        if index < ladder.len() {
            ladder.get(index).map(|node| (node, 0))
        } else if ladder.last().unwrap() == node {
            None
        } else {
            Some((ladder.last().unwrap(), index + 1 - ladder.len()))
        }
    }

    pub fn get_ancestor(&self, node: &NodeId, delta_depth: usize) -> Option<&NodeId> {
        match self.follow_ladder(node, delta_depth) {
            Some((ancestor, 0)) => Some(ancestor),
            Some((ancestor, new_delta_depth)) => self.get_ancestor(ancestor, new_delta_depth),
            None => None,
        }
    }
}

pub struct LadderLABuilder {}

impl LevelAncestorBuilder<LadderLA> for LadderLABuilder {
    fn new() -> Self {
        LadderLABuilder {}
    }

    fn build(self, tree: &Tree, preprocessor: Preprocessor) -> LadderLA {
        LadderLA::build(tree, preprocessor)
    }
}

pub struct LadderLA {
    ladder_manager: LadderManager,
    preprocessor: Preprocessor,
}

impl LadderLA {
    pub fn build(tree: &Tree, preprocessor: Preprocessor) -> LadderLA {
        LadderLA {
            ladder_manager: LadderManager::build(tree, 2),
            preprocessor,
        }
    }

    pub fn get_ancestor(&self, node: &NodeId, delta_depth: usize) -> Option<&NodeId> {
        self.ladder_manager.get_ancestor(node, delta_depth)
    }
}

impl LevelAncestor for LadderLA {
    fn query(&self, node: &NodeId, depth: usize) -> Option<&NodeId> {
        match self
            .preprocessor
            .depth(node)
            .map(|node_depth| node_depth.checked_sub(depth))
            .flatten()
        {
            Some(relative_depth) => self.get_ancestor(node, relative_depth),
            None => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::level_ancestor::preprocessor::Preprocessor;
    use crate::level_ancestor::table::TableLA;
    use crate::tree_generator::balanced_tree::generate_balanced_tree;

    fn build_la(depth: usize, branching: usize) -> (Tree, LadderLA) {
        let t = generate_balanced_tree(depth, branching);
        let preprocessor = Preprocessor::build(&t);
        let la = LadderLA::build(&t, preprocessor);
        (t, la)
    }

    #[test]
    fn leaves_are_first_in_ladder() {
        let (t, ladder_la) = build_la(5, 2);
        for ladder in ladder_la.ladder_manager.ladders {
            let node = &t[*ladder.first().unwrap()];
            assert_eq!(node.children().len(), 0);
        }
    }

    #[test]
    fn node_height_is_index() {
        let (t, ladder_la) = build_la(5, 2);
        for node in t.nodes().iter().map(|node| *node.id()) {
            let node_ladder_index = ladder_la.ladder_manager.node_ladder_index[node];
            let node_height = ladder_la.ladder_manager.node_height[node];
            assert_eq!(
                node,
                ladder_la.ladder_manager.ladders[node_ladder_index][node_height]
            );
            let (query_result, _) = ladder_la.ladder_manager.follow_ladder(&node, 0).unwrap();
            assert_eq!(&node, query_result);
        }
    }

    #[test]
    fn leaves_have_zero_height() {
        let (t, ladder_la) = build_la(5, 2);
        let node_heights = t
            .nodes()
            .iter()
            .filter(|node| node.children().len() == 0)
            .map(|node| {
                ladder_la
                    .ladder_manager
                    .node_height
                    .get(*node.id())
                    .unwrap()
            });
        for height in node_heights {
            assert_eq!(*height, 0);
        }
    }

    #[test]
    fn get_ancestor_can_return_leaves() {
        let (t, ladder_la) = build_la(5, 2);
        let mut any_leaves = false;
        for node in t.nodes().iter().filter(|node| node.children().len() == 0) {
            assert_eq!(ladder_la.get_ancestor(node.id(), 0), Some(node.id()));
            any_leaves = true;
        }
        assert_eq!(any_leaves, true);
    }

    #[test]
    fn get_ancestor_can_return_own_node() {
        let (t, ladder_la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(ladder_la.get_ancestor(node.id(), 0), Some(node.id()));
        }
    }

    #[test]
    fn get_ancestor_can_return_parent() {
        let (t, ladder_la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(
                ladder_la.get_ancestor(node.id(), 1).cloned(),
                *node.parent()
            );
        }
    }

    #[test]
    fn query_at_same_depth_returns_node() {
        let (t, ladder_la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(
                ladder_la.query(node.id(), *ladder_la.preprocessor.depth(node.id()).unwrap()),
                Some(node.id())
            );
        }
    }

    #[test]
    fn root_is_depth_zero_same_ladder() {
        let (t, ladder_la) = build_la(5, 2);
        for node in t.nodes() {
            if ladder_la
                .ladder_manager
                .node_ladder(node.id())
                .unwrap()
                .last()
                .cloned()
                == *t.root()
            {
                assert_eq!(ladder_la.query(node.id(), 0).cloned(), *t.root());
            }
        }
    }

    #[test]
    fn root_is_depth_zero() {
        let (t, ladder_la) = build_la(3, 2);
        for node in t.nodes() {
            assert_eq!(ladder_la.query(node.id(), 0), Some(&t.root().unwrap()));
        }
    }

    #[test]
    fn deeper_depth_fails() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(
                la.query(node.id(), la.preprocessor.depth(node.id()).unwrap() + 1),
                None
            );
        }
    }

    #[test]
    fn query_behaves_like_table_query() {
        let (t, la) = build_la(5, 4);
        let table = TableLA::build(&t, Preprocessor::build(&t));

        for node in t.nodes() {
            for depth in 0..(la.preprocessor.depth(node.id()).unwrap() + 1) {
                assert_eq!(table.query(node.id(), depth), la.query(node.id(), depth));
            }
        }
    }
}
