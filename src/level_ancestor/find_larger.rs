use super::preprocessor::Preprocessor;
use super::{LevelAncestor, LevelAncestorBuilder};
use crate::graph::{NodeId, NodeIdVec, Tree};
use crate::offset_vec::OffsetVec;
use crate::search::{EulerTourSearch, IteratorAdapter};

const fn num_bits<T>() -> usize {
    std::mem::size_of::<T>() * 8
}

pub fn greatest_dividing_power_of_2(number: usize) -> usize {
    number & !(number - 1)
}

pub fn greatest_power_of_2_no_greater(number: usize) -> usize {
    1 << (num_bits::<usize>() as u32 - number.leading_zeros() - 1)
}

pub struct EulerTour {
    node_sequence: Vec<NodeId>,
    inverse_depth: Vec<usize>,
    representative: NodeIdVec<usize>,
    max_depth: usize,
}

impl EulerTour {
    pub fn new(t: &Tree, preprocessor: &Preprocessor) -> Self {
        let node_sequence: Vec<NodeId> =
            IteratorAdapter::new(EulerTourSearch::new(t.root().unwrap(), t), t).collect();
        let mut representative = NodeIdVec(vec![0; t.nodes().len()]);
        for (index, node) in node_sequence.iter().enumerate() {
            representative[*node] = index;
        }
        let depth_sequence: Vec<usize> = node_sequence
            .iter()
            .map(|id| preprocessor.depth(id).unwrap())
            .cloned()
            .collect();
        let max_depth = depth_sequence.iter().max().cloned().unwrap();
        EulerTour {
            node_sequence,
            inverse_depth: depth_sequence
                .into_iter()
                .map(|depth| max_depth - depth)
                .collect(),
            representative,
            max_depth,
        }
    }
}

struct ValleyRange {
    index: usize,
    low: usize,
    high: usize,
}

impl ValleyRange {
    pub fn new(index: usize, low: usize, high: usize) -> Self {
        ValleyRange { index, low, high }
    }
}

struct ValleySequence {
    valley: Vec<usize>,
    weight: Vec<usize>,
}

impl ValleySequence {
    pub fn new(tour: &EulerTour) -> Self {
        let mut stack = vec![ValleyRange::new(0, 0, tour.max_depth + 2)];
        let mut valley_sequence = vec![0usize; tour.node_sequence.len()];
        let mut weight = vec![0; tour.node_sequence.len()];
        // The depth of each node is offset by one so 0 is always strictly less than the depth of a node
        for (valley, (index, depth)) in valley_sequence
            .iter_mut()
            .zip(tour.inverse_depth.iter().map(|depth| depth + 1).enumerate())
        {
            while stack.last().unwrap().low >= depth {
                stack.pop();
            }
            if stack.last().unwrap().high >= depth {
                *valley = index;
                if stack.last().unwrap().high > depth {
                    stack.push(ValleyRange::new(index, depth, depth));
                }
            } else {
                while stack[stack.len() - 2].high < depth {
                    stack.pop();
                }
                *valley = stack.last().unwrap().index;
                if stack[stack.len() - 2].high > depth {
                    stack.last_mut().unwrap().high = depth;
                } else {
                    stack.pop();
                }
            }
            weight[*valley] += 1;
        }
        ValleySequence {
            valley: valley_sequence,
            weight,
        }
    }
}

struct LadderJump {
    jumps: Vec<usize>,
    ladders: Vec<Option<OffsetVec<usize>>>,
}

impl LadderJump {
    pub fn new(
        tour: &EulerTour,
        valley_sequence: &ValleySequence,
        ladder_multiplier: usize,
    ) -> Self {
        let mut right_sight: Vec<Option<usize>> = vec![None; tour.max_depth + 1];
        let mut jumps: Vec<usize> = vec![0; tour.inverse_depth.len()];
        let mut ladders: Vec<Option<OffsetVec<usize>>> = vec![None; tour.inverse_depth.len()];
        for (index, depth) in tour.inverse_depth.iter().enumerate().rev() {
            right_sight[*depth] = Some(index);
            let weight = valley_sequence.weight[index];
            if weight > 0 {
                // Someone has this index as valley
                let ladder_depth =
                    std::cmp::min(tour.max_depth - depth + 1, weight * ladder_multiplier);
                let ladder: Option<Vec<usize>> = right_sight[*depth..*depth + ladder_depth]
                    .iter()
                    .cloned()
                    .collect();
                ladders[index] = Some(OffsetVec::new(ladder.unwrap(), *depth));
            }
            if index == 0 {
                jumps[index] = 0;
            } else {
                jumps[index] = right_sight
                    [std::cmp::min(tour.max_depth, depth + greatest_dividing_power_of_2(index))]
                .unwrap();
            }
        }
        LadderJump { jumps, ladders }
    }
}

struct FLFields {
    ladders_and_jumps: LadderJump,
    valleys: ValleySequence,
    tour: EulerTour,
}

impl FLFields {
    pub fn new(tree: &Tree, preprocessor: Preprocessor, ladder_multiplier: usize) -> Self {
        let tour = EulerTour::new(tree, &preprocessor);
        let valleys = ValleySequence::new(&tour);
        let ladders_and_jumps = LadderJump::new(&tour, &valleys, ladder_multiplier);
        FLFields {
            ladders_and_jumps,
            valleys,
            tour,
        }
    }
}

pub trait FLLA {
    fn tour(&self) -> &EulerTour;
    fn find_jump(&self, index: usize, delta_inverse_depth: usize) -> usize;
    fn follow_jump(&self, index: usize) -> usize;
    fn ladder(&self, index: usize) -> &OffsetVec<usize>;
    fn inverse_depth(&self, index: usize) -> usize {
        self.tour().inverse_depth[index]
    }
    fn find_larger(&self, index: usize, inverse_depth: usize) -> Option<usize> {
        let local_inverse_depth = self.inverse_depth(index);
        if inverse_depth > self.tour().max_depth || local_inverse_depth > inverse_depth {
            return None;
        }
        if let Some(result) = self.ladder(index).get(inverse_depth) {
            return Some(*result);
        }
        let jump_id = self.find_jump(index, inverse_depth - local_inverse_depth);
        self.ladder(self.follow_jump(jump_id))
            .get(inverse_depth)
            .cloned()
    }
}

impl<T> LevelAncestor for T
where
    T: FLLA,
{
    fn query(&self, node: &NodeId, depth: usize) -> Option<&NodeId> {
        if let Some(index) = self.tour().representative.get(*node) {
            match self.find_larger(*index, self.tour().max_depth - depth) {
                Some(new_index) => self.tour().node_sequence.get(new_index),
                None => None,
            }
        } else {
            None
        }
    }
}

pub struct ForwardFL(FLFields);

impl ForwardFL {
    pub fn new(tree: &Tree, preprocessor: Preprocessor) -> Self {
        ForwardFL(FLFields::new(tree, preprocessor, 5))
    }
}

pub struct ForwardFLBuilder();

impl LevelAncestorBuilder<ForwardFL> for ForwardFLBuilder {
    fn new() -> Self {
        ForwardFLBuilder()
    }

    fn build(self, tree: &Tree, preprocessor: Preprocessor) -> ForwardFL {
        ForwardFL::new(tree, preprocessor)
    }
}

impl FLLA for ForwardFL {
    fn tour(&self) -> &EulerTour {
        &self.0.tour
    }

    fn follow_jump(&self, index: usize) -> usize {
        self.0.ladders_and_jumps.jumps[index]
    }

    fn find_jump(&self, index: usize, delta_inverse_depth: usize) -> usize {
        let jump_height = greatest_power_of_2_no_greater(delta_inverse_depth);
        let jump_index = index / jump_height * jump_height;
        if jump_index % (2 * jump_height) == 0 {
            jump_index + jump_height
        } else {
            jump_index
        }
    }
    fn ladder(&self, index: usize) -> &OffsetVec<usize> {
        self.0.ladders_and_jumps.ladders[self.0.valleys.valley[index]]
            .as_ref()
            .unwrap()
    }
}

pub struct StandardFL(FLFields);

impl StandardFL {
    pub fn new(tree: &Tree, preprocessor: Preprocessor) -> Self {
        StandardFL(FLFields::new(tree, preprocessor, 8))
    }
}

impl FLLA for StandardFL {
    fn tour(&self) -> &EulerTour {
        &self.0.tour
    }

    fn follow_jump(&self, index: usize) -> usize {
        self.0.ladders_and_jumps.jumps[index]
    }

    fn find_jump(&self, index: usize, delta_inverse_depth: usize) -> usize {
        let jump_height = greatest_power_of_2_no_greater(delta_inverse_depth / 3);
        let mut jump_index = index / jump_height * jump_height;
        if jump_index % (2 * jump_height) == 0 {
            jump_index -= jump_height
        }
        jump_index
    }
    fn ladder(&self, index: usize) -> &OffsetVec<usize> {
        self.0.ladders_and_jumps.ladders[self.0.valleys.valley[index]]
            .as_ref()
            .unwrap()
    }
}

pub struct StandardFLBuilder();

impl LevelAncestorBuilder<StandardFL> for StandardFLBuilder {
    fn new() -> Self {
        StandardFLBuilder()
    }

    fn build(self, tree: &Tree, preprocessor: Preprocessor) -> StandardFL {
        StandardFL::new(tree, preprocessor)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::level_ancestor::preprocessor::Preprocessor;
    use crate::level_ancestor::table::TableLA;
    use crate::tree_generator::balanced_tree::generate_balanced_tree;

    fn build_la_standard(depth: usize, branching: usize) -> (Tree, StandardFL) {
        let t = generate_balanced_tree(depth, branching);
        let preprocessor = Preprocessor::build(&t);
        let la = StandardFL::new(&t, preprocessor);
        (t, la)
    }
    fn build_la_forward(depth: usize, branching: usize) -> (Tree, ForwardFL) {
        let t = generate_balanced_tree(depth, branching);
        let preprocessor = Preprocessor::build(&t);
        let la = ForwardFL::new(&t, preprocessor);
        (t, la)
    }

    #[test]
    fn ladder_length() {
        let (_, la) = build_la_standard(10, 2);
        for (inverse_depth, (weight, ladder)) in la.0.tour.inverse_depth.iter().zip(
            la.0.valleys
                .weight
                .iter()
                .zip(la.0.ladders_and_jumps.ladders.iter()),
        ) {
            let expected_ladder_depth =
                std::cmp::min(la.tour().max_depth - *inverse_depth + 1, 8 * weight);
            if *weight == 0 {
                assert!(ladder.is_none());
            } else {
                assert_eq!(ladder.as_ref().unwrap().data().len(), expected_ladder_depth);
            }
        }

        for i in la.0.valleys.valley {
            assert!(la.0.valleys.weight[i] != 0);
        }
    }

    #[test]
    fn query_behaves_like_table_query_standard() {
        let (t, la) = build_la_standard(5, 4);
        let table = TableLA::build(&t, Preprocessor::build(&t));

        for node in t.nodes() {
            for depth in 0..la.tour().max_depth {
                assert_eq!(table.query(node.id(), depth), la.query(node.id(), depth));
            }
        }
    }

    #[test]
    fn query_behaves_like_table_query_forward() {
        let (t, la) = build_la_forward(5, 4);
        let table = TableLA::build(&t, Preprocessor::build(&t));

        for node in t.nodes() {
            for depth in 0..la.tour().max_depth {
                assert_eq!(table.query(node.id(), depth), la.query(node.id(), depth));
            }
        }
    }

    #[test]
    fn query_behaves_like_table_query_big() {
        {
            let (t, la) = build_la_forward(10, 2);
            let table = TableLA::build(&t, Preprocessor::build(&t));
            for node in t.nodes() {
                for depth in 0..la.tour().max_depth {
                    println!("{:?}, {}", node.id(), depth);
                    assert_eq!(table.query(node.id(), depth), la.query(node.id(), depth));
                }
            }
        }
        {
            let (t, la) = build_la_standard(10, 2);
            let table = TableLA::build(&t, Preprocessor::build(&t));
            for node in t.nodes() {
                for depth in 0..la.tour().max_depth {
                    assert_eq!(table.query(node.id(), depth), la.query(node.id(), depth));
                }
            }
        }
    }
}
