use super::preprocessor::Preprocessor;
use super::{LevelAncestor, LevelAncestorBuilder};
use crate::graph::{NodeId, NodeIdVec, Tree};
use crate::search::{EulerTourSearch, IteratorAdapter};

pub struct TableLABuilder {}

impl LevelAncestorBuilder<TableLA> for TableLABuilder {
    fn new() -> TableLABuilder {
        TableLABuilder {}
    }
    fn build(self, tree: &Tree, preprocessor: Preprocessor) -> TableLA {
        TableLA::build(tree, preprocessor)
    }
}

pub struct TableLA {
    level_ancestor_map: NodeIdVec<Vec<NodeId>>,
    preprocessor: Preprocessor,
}

impl TableLA {
    pub fn build(t: &Tree, preprocessor: Preprocessor) -> TableLA {
        let mut level_ancestor_map = NodeIdVec(vec![None; t.nodes().len()]);
        let mut ancestors = Vec::<NodeId>::new();
        let mut search = EulerTourSearch::new(t.root().unwrap(), t);
        while let Some(visit) = search.next(&t) {
            match search.withdrawn() {
                true => {
                    ancestors.pop();
                }
                false => {
                    ancestors.push(visit);
                    level_ancestor_map[visit] =
                        Some(ancestors.iter().rev().cloned().collect::<Vec<NodeId>>());
                }
            }
        }
        let level_ancestor_map_unwraped = NodeIdVec(
            level_ancestor_map
                .0
                .into_iter()
                .collect::<Option<Vec<Vec<NodeId>>>>()
                .unwrap(),
        );
        TableLA {
            level_ancestor_map: level_ancestor_map_unwraped,
            preprocessor,
        }
    }

    pub fn get_ancestor(&self, node: &NodeId, number: usize) -> Option<&NodeId> {
        self.level_ancestor_map
            .get(*node)
            .map(|ancestors| ancestors.get(number))
            .flatten()
    }
}

impl LevelAncestor for TableLA {
    fn query(&self, node: &NodeId, depth: usize) -> Option<&NodeId> {
        match self
            .preprocessor
            .depth(node)
            .map(|node_depth| node_depth.checked_sub(depth))
            .flatten()
        {
            Some(relative_depth) => self.get_ancestor(node, relative_depth),
            None => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::level_ancestor::preprocessor::Preprocessor;
    use crate::tree_generator::balanced_tree::generate_balanced_tree;

    fn build_la(depth: usize, branching: usize) -> (Tree, TableLA) {
        let t = generate_balanced_tree(depth, branching);
        let preprocessor = Preprocessor::build(&t);
        let la = TableLA::build(&t, preprocessor);
        (t, la)
    }

    #[test]
    fn root_is_last_in_all_lists() {
        let (t, la) = build_la(5, 2);
        for ancestor_list in la.level_ancestor_map.0 {
            assert_eq!(ancestor_list.last().unwrap(), &t.root().unwrap());
        }
    }

    #[test]
    fn first_element_in_all_lists_are_self() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(
                la.level_ancestor_map
                    .get(*node.id())
                    .unwrap()
                    .first()
                    .unwrap(),
                node.id()
            );
        }
    }

    #[test]
    fn second_element_is_parent() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes() {
            match node.parent() {
                Some(parent) => {
                    assert_eq!(la.level_ancestor_map.get(*node.id()).unwrap()[1], *parent)
                }
                _ => (),
            }
        }
    }

    #[test]
    fn length_of_list_is_depth_of_node_plus_one() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(
                la.level_ancestor_map.get(*node.id()).unwrap().len(),
                la.preprocessor.depth(node.id()).unwrap() + 1
            );
        }
    }

    #[test]
    fn root_is_depth_zero() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(la.query(node.id(), 0), Some(&t.root().unwrap()));
        }
    }

    #[test]
    fn deeper_depth_fails() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(
                la.query(node.id(), la.preprocessor.depth(node.id()).unwrap() + 1),
                None
            );
        }
    }
}
