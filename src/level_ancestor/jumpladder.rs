use super::jump::JumpManager;
use super::ladder::LadderManager;
use super::preprocessor::Preprocessor;
use super::{LevelAncestor, LevelAncestorBuilder};
use crate::graph::{NodeId, Tree};

pub struct JumpLadderLABuilder {}

impl LevelAncestorBuilder<JumpLadderLA> for JumpLadderLABuilder {
    fn new() -> JumpLadderLABuilder {
        JumpLadderLABuilder {}
    }

    fn build(self, tree: &Tree, preprocessor: Preprocessor) -> JumpLadderLA {
        JumpLadderLA::build(tree, preprocessor)
    }
}

pub struct JumpLadderLA {
    jump_manager: JumpManager,
    ladder_manager: LadderManager,
    preprocessor: Preprocessor,
}

impl JumpLadderLA {
    pub fn build(g: &Tree, preprocessor: Preprocessor) -> JumpLadderLA {
        JumpLadderLA {
            jump_manager: JumpManager::build(g),
            ladder_manager: LadderManager::build(g, 2),
            preprocessor,
        }
    }

    pub fn get_ancestor(&self, node: &NodeId, number: usize) -> Option<&NodeId> {
        match self.jump_manager.jump(node, number) {
            Some((new_node, 0)) => Some(new_node),
            Some((new_node, new_number)) => self
                .ladder_manager
                .follow_ladder(new_node, new_number)
                .map(|(node, _)| node),
            None => None,
        }
    }
}

impl LevelAncestor for JumpLadderLA {
    fn query(&self, node: &NodeId, depth: usize) -> Option<&NodeId> {
        match self
            .preprocessor
            .depth(node)
            .map(|node_depth| node_depth.checked_sub(depth))
            .flatten()
        {
            Some(relative_depth) => self.get_ancestor(node, relative_depth),
            None => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::level_ancestor::preprocessor::Preprocessor;
    use crate::level_ancestor::table::TableLA;
    use crate::tree_generator::balanced_tree::generate_balanced_tree;

    fn build_la(depth: usize, branching: usize) -> (Tree, JumpLadderLA) {
        let t = generate_balanced_tree(depth, branching);
        let preprocessor = Preprocessor::build(&t);
        let la = JumpLadderLA::build(&t, preprocessor);
        (t, la)
    }

    #[test]
    fn get_ancestor_can_return_leaves() {
        let (t, la) = build_la(5, 2);
        let mut any_leaves = false;
        for node in t.nodes().iter().filter(|node| node.children().len() == 0) {
            assert_eq!(la.get_ancestor(node.id(), 0), Some(node.id()));
            any_leaves = true;
        }
        assert_eq!(any_leaves, true);
    }

    #[test]
    fn get_ancestor_can_return_own_node() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(la.get_ancestor(node.id(), 0), Some(node.id()));
        }
    }

    #[test]
    fn get_ancestor_can_return_parent() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(la.get_ancestor(node.id(), 1).cloned(), *node.parent());
        }
    }

    #[test]
    fn query_at_same_depth_returns_node() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(
                la.query(node.id(), *la.preprocessor.depth(node.id()).unwrap()),
                Some(node.id())
            );
        }
    }

    #[test]
    fn root_is_depth_zero_same_ladder() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes() {
            if la
                .ladder_manager
                .node_ladder(node.id())
                .unwrap()
                .last()
                .cloned()
                == *t.root()
            {
                assert_eq!(la.query(node.id(), 0).cloned(), *t.root());
            }
        }
    }

    #[test]
    fn root_is_depth_zero() {
        let (t, la) = build_la(3, 2);
        for node in t.nodes() {
            assert_eq!(la.query(node.id(), 0), Some(&t.root().unwrap()));
        }
    }

    #[test]
    fn deeper_depth_fails() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(
                la.query(node.id(), la.preprocessor.depth(node.id()).unwrap() + 1),
                None
            );
        }
    }

    #[test]
    fn query_behaves_like_table_query() {
        let (t, la) = build_la(5, 4);
        let table = TableLA::build(&t, Preprocessor::build(&t));

        for node in t.nodes() {
            for depth in 0..(la.preprocessor.depth(node.id()).unwrap() + 1) {
                assert_eq!(table.query(node.id(), depth), la.query(node.id(), depth));
            }
        }
    }
}
