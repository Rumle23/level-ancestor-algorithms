use super::find_larger::{greatest_dividing_power_of_2, greatest_power_of_2_no_greater};
use super::ladder::LadderManager;
use super::preprocessor::Preprocessor;
use super::{LevelAncestor, LevelAncestorBuilder};
use crate::graph::{NodeId, NodeIdVec, Tree};
use crate::search::{EulerTourSearch, IteratorAdapter};

pub struct OneLevelTreeBuilder {}

impl LevelAncestorBuilder<OneLevelTreeLA> for OneLevelTreeBuilder {
    fn new() -> OneLevelTreeBuilder {
        OneLevelTreeBuilder {}
    }
    fn build(self, tree: &Tree, preprocessor: Preprocessor) -> OneLevelTreeLA {
        OneLevelTreeLA::build(tree, preprocessor)
    }
}

pub struct OneLevelTreeLA {
    representative: NodeIdVec<u32>,
    jumps: Vec<NodeId>,
    ladders: LadderManager,
    preprocessor: Preprocessor,
}

impl OneLevelTreeLA {
    pub fn build(g: &Tree, preprocessor: Preprocessor) -> OneLevelTreeLA {
        let mut ancestors = vec![None; g.nodes().len()];
        let mut jumps = vec![g.root().unwrap(); g.nodes().len()];
        let mut representative: NodeIdVec<u32> = NodeIdVec(vec![0; g.nodes().len()]);
        let mut euler_iterator =
            IteratorAdapter::new(EulerTourSearch::new(g.root().unwrap(), &g), &g);
        //skip root node
        euler_iterator.next();
        let mut euler_num: usize = 1;
        let mut even = false;
        ancestors[0] = *g.root();
        for (node, depth) in euler_iterator.map(|node| (node, preprocessor.depth(&node).unwrap())) {
            ancestors[*depth] = Some(node);
            representative[node] = euler_num as u32;
            if even {
                let jump_height = greatest_dividing_power_of_2(euler_num);
                jumps[euler_num / 2] =
                    ancestors[depth.checked_sub(jump_height).unwrap_or(0)].unwrap();
            }
            euler_num += 1;
            even = !even;
        }
        OneLevelTreeLA {
            representative,
            jumps,
            ladders: LadderManager::build(g, 5),
            preprocessor,
        }
    }

    fn find_jump(&self, index: u32, delta_depth: usize) -> u32 {
        let jump_height = (greatest_power_of_2_no_greater(delta_depth) as u32) / 2;
        let jump_index = index / jump_height * jump_height;
        if jump_index % (2 * jump_height) == 0 {
            (jump_index + jump_height) / 2
        } else {
            (jump_index) / 2
        }
    }

    pub fn get_ancestor(&self, node: &NodeId, depth: usize) -> Option<&NodeId> {
        let node_depth = self.preprocessor.depth(node)?;
        if node_depth < &depth {
            return None;
        }

        let delta_depth = node_depth - depth;
        if let Some((ancestor, 0)) = self.ladders.follow_ladder(node, delta_depth) {
            Some(ancestor)
        } else {
            let jump_index = self.find_jump(self.representative[*node], delta_depth);
            let jump_node = self.jumps[jump_index as usize];
            let jump_depth = self.preprocessor.depth(&jump_node)?;
            self.ladders
                .follow_ladder(&jump_node, delta_depth + jump_depth - node_depth)
                .map(|(ancestor, _)| ancestor)
        }
    }
}

impl LevelAncestor for OneLevelTreeLA {
    fn query(&self, node: &NodeId, depth: usize) -> Option<&NodeId> {
        self.get_ancestor(node, depth)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::level_ancestor::TableLA;
    use crate::tree_generator::balanced_tree::generate_balanced_tree;
    fn build_la(depth: usize, branching: usize) -> (Tree, OneLevelTreeLA) {
        let t = generate_balanced_tree(depth, branching);
        let preprocessor = Preprocessor::build(&t);
        let la = OneLevelTreeBuilder::new().build(&t, preprocessor);
        (t, la)
    }

    #[test]
    fn ladder_manager_height_test() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes().iter().map(|node| *node.id()) {
            let (query_result, _) = la.ladders.follow_ladder(&node, 0).unwrap();
            assert_eq!(&node, query_result);
        }
    }

    #[test]
    fn query_at_same_depth_returns_node() {
        let (t, la) = build_la(5, 4);
        for node in t.nodes() {
            assert_eq!(
                node.id(),
                la.query(node.id(), *la.preprocessor.depth(node.id()).unwrap())
                    .unwrap()
            );
        }
    }

    #[test]
    fn query_behaves_like_table_query_on_ladder() {
        let (t, la) = build_la(5, 4);
        let table = TableLA::build(&t, Preprocessor::build(&t));

        for node in t.nodes() {
            let node_depth = la.preprocessor.depth(node.id()).unwrap();
            for depth in node_depth.checked_sub(4).unwrap_or(0)..node_depth + 1 {
                assert_eq!(table.query(node.id(), depth), la.query(node.id(), depth));
            }
        }
    }

    #[test]
    fn query_behaves_like_table_query() {
        let (t, la) = build_la(5, 4);
        let table = TableLA::build(&t, Preprocessor::build(&t));

        for node in t.nodes() {
            for depth in 0..(la.preprocessor.depth(node.id()).unwrap() + 1) {
                assert_eq!(table.query(node.id(), depth), la.query(node.id(), depth));
            }
        }
    }

    #[test]
    fn query_behaves_like_table_when_jumping() {
        let (t, la) = build_la(10, 2);
        let table = TableLA::build(&t, Preprocessor::build(&t));

        for node in t.nodes() {
            for depth in 0..(la
                .preprocessor
                .depth(node.id())
                .unwrap()
                .checked_sub(5)
                .unwrap_or(0))
            {
                assert_eq!(table.query(node.id(), depth), la.query(node.id(), depth));
            }
        }
    }
}
