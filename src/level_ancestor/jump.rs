use super::preprocessor::Preprocessor;
use super::{LevelAncestor, LevelAncestorBuilder};
use crate::graph::{NodeId, NodeIdVec, Tree};
use std::mem::size_of;

#[derive(Clone)]
pub struct JumpVec(Vec<NodeId>);

impl JumpVec {
    pub fn new() -> JumpVec {
        JumpVec(Vec::new())
    }
    pub fn add_jump(&mut self, node: NodeId) {
        self.0.push(node);
    }

    pub fn jump(&self, number: usize) -> Option<(&NodeId, usize)> {
        if number == 0 {
            self.0.first().map(|n| (n, 0))
        } else {
            let most_significant_set_bit = size_of::<usize>() * 8 - number.leading_zeros() as usize;
            match self.0.get(most_significant_set_bit) {
                Some(jump_node) => {
                    Some((jump_node, number - (1 << (most_significant_set_bit - 1))))
                }
                None => None,
            }
        }
    }
}

pub struct JumpManager {
    jump_maps: NodeIdVec<JumpVec>,
}

impl JumpManager {
    pub fn build(g: &Tree) -> JumpManager {
        let mut level_ancestor_map = NodeIdVec(vec![JumpVec::new(); g.nodes().len()]);
        for node in g {
            let node_ancestor_map = level_ancestor_map.get_mut(*node.id()).unwrap();
            node_ancestor_map.add_jump(*node.id());
            match node.parent() {
                Some(parent) => node_ancestor_map.add_jump(*parent),
                None => (),
            }
        }
        let mut jump_manager = JumpManager {
            jump_maps: level_ancestor_map,
        };
        for jump in 0..((g.nodes().len() - 1) as f32).log2().ceil() as usize {
            for node in g {
                let next_jump_ancestor = jump_manager
                    .get_ancestor(node.id(), 1 << jump)
                    .and_then(|ancestor| jump_manager.get_ancestor(&ancestor, 1 << jump))
                    .copied();
                if let Some(ancestor) = next_jump_ancestor {
                    jump_manager.add_jump(*node.id(), ancestor);
                }
            }
        }
        jump_manager
    }

    fn add_jump(&mut self, node: NodeId, jump: NodeId) {
        self.jump_maps.get_mut(node).unwrap().add_jump(jump);
    }

    pub fn jump(&self, node: &NodeId, number: usize) -> Option<(&NodeId, usize)> {
        match self
            .jump_maps
            .get(*node)
            .map(|ancestors| ancestors.jump(number))
            .flatten()
        {
            Some((jump_node, remaining)) => Some((jump_node, remaining)),
            None => None,
        }
    }

    pub fn get_ancestor(&self, node: &NodeId, number: usize) -> Option<&NodeId> {
        match self.jump(node, number) {
            Some((jump_node, 0)) => Some(jump_node),
            Some((jump_node, new_number)) => self.get_ancestor(jump_node, new_number),
            None => None,
        }
    }
}

pub struct JumpLABuilder {}
impl LevelAncestorBuilder<JumpLA> for JumpLABuilder {
    fn new() -> Self {
        JumpLABuilder {}
    }

    fn build(self, tree: &Tree, preprocessor: Preprocessor) -> JumpLA {
        JumpLA::build(tree, preprocessor)
    }
}

pub struct JumpLA {
    jump_manager: JumpManager,
    preprocessor: Preprocessor,
}

impl JumpLA {
    pub fn build(g: &Tree, pre: Preprocessor) -> JumpLA {
        JumpLA {
            jump_manager: JumpManager::build(g),
            preprocessor: pre,
        }
    }

    pub fn get_ancestor(&self, node: &NodeId, number: usize) -> Option<&NodeId> {
        self.jump_manager.get_ancestor(node, number)
    }
}

impl LevelAncestor for JumpLA {
    fn query(&self, node: &NodeId, depth: usize) -> Option<&NodeId> {
        match self
            .preprocessor
            .depth(node)
            .map(|node_depth| node_depth.checked_sub(depth))
            .flatten()
        {
            Some(relative_depth) => self.get_ancestor(node, relative_depth),
            None => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::level_ancestor::preprocessor::Preprocessor;
    use crate::level_ancestor::table::TableLA;
    use crate::tree_generator::balanced_tree::generate_balanced_tree;

    fn build_la(depth: usize, branching: usize) -> (Tree, JumpLA) {
        let t = generate_balanced_tree(depth, branching);
        let preprocessor = Preprocessor::build(&t);
        let la = JumpLABuilder::new().build(&t, preprocessor);
        (t, la)
    }

    #[test]
    fn get_ancestor_can_return_own_node() {
        let (t, jump_la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(jump_la.get_ancestor(node.id(), 0), Some(node.id()));
        }
    }

    #[test]
    fn get_ancestor_can_return_parent() {
        let (t, jump_la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(jump_la.get_ancestor(node.id(), 1).cloned(), *node.parent());
        }
    }

    #[test]
    fn query_at_same_depth_returns_node() {
        let (t, jump_la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(
                jump_la.query(node.id(), *jump_la.preprocessor.depth(node.id()).unwrap()),
                Some(node.id())
            );
        }
    }

    #[test]
    fn root_is_depth_zero_jump() {
        let (t, jump_la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(jump_la.query(node.id(), 0), Some(&t.root().unwrap()));
        }
    }

    #[test]
    fn deeper_depth_fails() {
        let (t, la) = build_la(5, 2);
        for node in t.nodes() {
            assert_eq!(
                la.query(node.id(), la.preprocessor.depth(node.id()).unwrap() + 1),
                None
            );
        }
    }

    #[test]
    fn query_behaves_like_table_query() {
        let (t, la) = build_la(5, 4);
        let table = TableLA::build(&t, Preprocessor::build(&t));

        for node in t.nodes() {
            for depth in 0..(la.preprocessor.depth(node.id()).unwrap() + 1) {
                assert_eq!(table.query(node.id(), depth), la.query(node.id(), depth));
            }
        }
    }
}
