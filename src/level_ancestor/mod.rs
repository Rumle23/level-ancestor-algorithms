mod find_larger;
mod jump;
mod jumpladder;
mod ladder;
mod macromicro;
mod one_level_tree;
mod preprocessor;
mod table;
use crate::graph::{NodeId, Tree};
pub use find_larger::{ForwardFL, ForwardFLBuilder, StandardFL, StandardFLBuilder};
pub use jump::{JumpLA, JumpLABuilder};
pub use jumpladder::{JumpLadderLA, JumpLadderLABuilder};
pub use ladder::{LadderLA, LadderLABuilder};
pub use macromicro::{MacroMicroLA, MacroMicroLABuilder};
pub use one_level_tree::{OneLevelTreeBuilder, OneLevelTreeLA};
pub use preprocessor::Preprocessor;
pub use table::{TableLA, TableLABuilder};

pub trait LevelAncestor {
    fn query(&self, node: &NodeId, depth: usize) -> Option<&NodeId>;
}

pub trait LevelAncestorBuilder<T: LevelAncestor> {
    fn new() -> Self;
    fn build(self, tree: &Tree, pre: Preprocessor) -> T;
}
