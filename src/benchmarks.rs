use crate::graph::{NodeId, Tree};
use crate::level_ancestor::{
    ForwardFLBuilder, JumpLABuilder, JumpLadderLABuilder, LadderLABuilder, LevelAncestor,
    LevelAncestorBuilder, MacroMicroLABuilder, OneLevelTreeBuilder, Preprocessor,
    StandardFLBuilder, TableLABuilder,
};
use crate::test_suite::{bench, BenchResult};
use std::time::Duration;

pub struct TestSpec {
    tree: Tree,
    queries: Vec<(NodeId, usize)>,
    query_multiplier: usize,
}

impl TestSpec {
    pub fn new(tree: Tree) -> Self {
        TestSpec {
            tree,
            queries: Vec::new(),
            query_multiplier: 1,
        }
    }

    pub fn add_queries<'a>(&'a mut self, mut queries: Vec<(NodeId, usize)>) -> &'a mut Self {
        self.queries.append(&mut queries);
        self
    }

    pub fn set_query_multiplier<'a>(&'a mut self, mult: usize) -> &'a mut Self {
        self.query_multiplier = mult;
        self
    }
}

pub fn bench_build<T: LevelAncestorBuilder<K>, K: LevelAncestor>(
    builder: T,
    tree: &Tree,
    pre: Preprocessor,
) -> (BenchResult, K) {
    bench(|| builder.build(tree, pre))
}

pub fn bench_query<T: LevelAncestor>(
    la: &T,
    node: &NodeId,
    depth: usize,
) -> (BenchResult, Option<NodeId>) {
    bench(|| la.query(node, depth).cloned())
}

pub fn sum_queries<T: LevelAncestor>(la: &T, queries: &Vec<(NodeId, usize)>) -> BenchResult {
    let mut sum = BenchResult::new(Duration::new(0, 0), 0, 0);
    for result in queries
        .iter()
        .map(|(node, depth)| bench_query(la, node, *depth))
        .map(|(benchresult, _)| benchresult)
    {
        sum += result;
    }
    sum
}

pub fn bench_spec<T: LevelAncestorBuilder<K>, K: LevelAncestor>(
    builder: T,
    ts: &TestSpec,
) -> (BenchResult, BenchResult) {
    let (build_res, la) = bench_build(builder, &ts.tree, Preprocessor::build(&ts.tree));
    let mut query_sum = BenchResult::new(Duration::new(0, 0), 0, 0);
    for _ in 0..ts.query_multiplier {
        query_sum += sum_queries(&la, &ts.queries);
    }
    (build_res, query_sum)
}

fn bench_and_print_la<T: LevelAncestorBuilder<K>, K: LevelAncestor>(
    builder: T,
    name: &str,
    ts: &TestSpec,
) {
    let (build, avg_sum) = bench_spec(builder, ts);
    // CSV friendly
    println!(
        "{}, {}, {}, {}, {}, {}",
        ts.tree.nodes().len(),
        name,
        build.time().as_millis(),
        build.max_memory() / (1024 * 1024),
        build.current_memory() / (1024 * 1024),
        avg_sum.time().as_millis()
    );

    // Human Friendly
    //println!(
    //    "{}, {}, {}, {}",
    //    ts.tree.nodes().len() / 1000,
    //    name,
    //    build,
    //    avg_sum
    //);
}

pub fn bench_and_print(ts: &TestSpec) {
    bench_and_print_la(TableLABuilder::new(), "TableLA", ts);
    bench_and_print_la(JumpLABuilder::new(), "JumpLA", ts);
    bench_and_print_la(LadderLABuilder::new(), "LadderLA", ts);
    bench_and_print_la(JumpLadderLABuilder::new(), "JumpLadderLA", ts);
    bench_and_print_la(MacroMicroLABuilder::new(), "MacroMicroLA", ts);
    bench_and_print_la(StandardFLBuilder::new(), "StandardFL", ts);
    bench_and_print_la(ForwardFLBuilder::new(), "ForwardFL", ts);
    bench_and_print_la(OneLevelTreeBuilder::new(), "OneLevelTree", ts);
}
