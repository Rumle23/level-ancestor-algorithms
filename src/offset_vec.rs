use std::ops::{Index, IndexMut};

#[derive(Clone)]
pub struct OffsetVec<T> {
    data: Vec<T>,
    offset: usize,
}

// Offsets the get query by offset
impl<T> OffsetVec<T> {
    pub fn new(data: Vec<T>, offset: usize) -> OffsetVec<T> {
        OffsetVec { data, offset }
    }
    pub fn data(&self) -> &Vec<T> {
        &self.data
    }

    pub fn data_mut(&mut self) -> &mut Vec<T> {
        &mut self.data
    }

    pub fn get(&self, index: usize) -> Option<&T> {
        self.data.get(index - self.offset)
    }

    pub fn get_mut(&mut self, index: usize) -> Option<&mut T> {
        self.data.get_mut(index - self.offset)
    }

    pub fn offset(&self) -> usize {
        self.offset
    }
}

impl<T> Index<usize> for OffsetVec<T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        &self.data[index - self.offset]
    }
}

impl<T> IndexMut<usize> for OffsetVec<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.data[index - self.offset]
    }
}
