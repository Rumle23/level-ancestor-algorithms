#![feature(atomic_min_max)]
#![feature(const_fn)]
#![allow(dead_code)]
mod benchmarks;
mod graph;
mod level_ancestor;
mod offset_vec;
mod search;
mod test_suite;
mod tree_generator;

use benchmarks::{bench_and_print, TestSpec};
use graph::Tree;
use tree_generator::balanced_tree::generate_balanced_tree;
use tree_generator::generate_all_queries;
use tree_generator::path_tree::generate_path_tree;
use tree_generator::random_binary_tree::random_binary_tree;
use tree_generator::random_path_tree_query_generator;
use tree_generator::random_select_query;

fn bench_tree_random(t: Tree, query_count: usize, seed: [u8; 32]) {
    let queries = generate_all_queries(&t);
    let rand_query = random_select_query(&queries, query_count, seed);
    let mut ts = TestSpec::new(t);
    ts.add_queries(rand_query);
    bench_and_print(&ts);
}

fn bench_single_path_random_query(depth: usize, query_count: usize, seed: [u8; 32]) {
    let t = generate_balanced_tree(depth - 1, 1);
    let queries = random_path_tree_query_generator(depth - 1, query_count, seed);
    let mut ts = TestSpec::new(t);
    ts.add_queries(queries);
    bench_and_print(&ts);
}

fn bench_path_tree_random_query(
    layers: usize,
    branching: usize,
    spacing: usize,
    query_count: usize,
    seed: [u8; 32],
) {
    bench_tree_random(
        generate_path_tree(layers, branching, spacing),
        query_count,
        seed,
    );
}

fn bench_random_tree_random_query(
    number_of_nodes: usize,
    tree_seed: [u8; 32],
    query_count: usize,
    query_seed: [u8; 32],
    skew: f64,
) {
    bench_tree_random(
        random_binary_tree(number_of_nodes, tree_seed, skew),
        query_count,
        query_seed,
    );
}

fn bench_balanced_tree_random_query(
    branch: usize,
    depth: usize,
    query_count: usize,
    query_seed: [u8; 32],
) {
    bench_tree_random(
        generate_balanced_tree(depth, branch),
        query_count,
        query_seed,
    );
}

fn bench_balanced_tree_no_query(depth: usize, branch: usize) {
    let t = generate_balanced_tree(depth, branch);
    assert!(t.nodes().len() > 0);
    let ts = TestSpec::new(t);
    bench_and_print(&ts);
}

fn main() {
    let query_count = 100 * 10usize.pow(6);
    let max_tree = 102400 / 4 * 10usize.pow(3);
    println!("Random split tree 100M queries");
    for i in 0..4 {
        let mut nodes = 10usize.pow(5);
        while nodes <= max_tree {
            bench_random_tree_random_query(nodes, [i; 32], query_count, [i; 32], 1.0);
            nodes *= 2;
        }
    }
    println!("Skewed Random split tree 100M queries");
    for skew in [2, 5, 10, 20, 50, 100].iter() {
        println!("Skew: 1/{}", skew);
        for i in 1..4 {
            bench_random_tree_random_query(
                1000000,
                [i; 32],
                query_count,
                [i; 32],
                1.0 / (*skew as f64),
            );
        }
    }
    println!("\n\nSingle Path tree 100M queries");
    let mut depth = 10usize.pow(5);
    while depth <= max_tree {
        bench_single_path_random_query(depth, query_count, [0; 32]);
        depth *= 2;
    }
}
