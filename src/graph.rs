use std::convert::TryInto;
use std::num::NonZeroU32;
use std::ops::{Index, IndexMut};
use std::slice::{Iter, IterMut};

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct NodeId(NonZeroU32);

impl NodeId {
    pub fn new(x: u32) -> Self {
        unsafe { NodeId(NonZeroU32::new_unchecked(x + 1)) }
    }
    pub fn index(&self) -> usize {
        (self.0.get() - 1) as usize
    }
}

impl<T> Index<NodeId> for Vec<T> {
    type Output = T;

    fn index(&self, node: NodeId) -> &Self::Output {
        self.index(node.index())
    }
}

impl<T> IndexMut<NodeId> for Vec<T> {
    fn index_mut(&mut self, node: NodeId) -> &mut Self::Output {
        self.index_mut(node.index())
    }
}

pub struct NodeIdVec<T>(pub Vec<T>);

impl<T> NodeIdVec<T> {
    pub fn get(&self, node: NodeId) -> Option<&T> {
        self.0.get(node.index())
    }

    pub fn get_mut(&mut self, node: NodeId) -> Option<&mut T> {
        self.0.get_mut(node.index())
    }
}

impl<T> Index<NodeId> for NodeIdVec<T> {
    type Output = T;

    fn index(&self, node: NodeId) -> &Self::Output {
        &self.0[node.index()]
    }
}

impl<T> IndexMut<NodeId> for NodeIdVec<T> {
    fn index_mut(&mut self, node: NodeId) -> &mut Self::Output {
        &mut self.0[node.index()]
    }
}

impl<T> std::ops::Deref for NodeIdVec<T> {
    type Target = Vec<T>;
    fn deref(&self) -> &Vec<T> {
        &self.0
    }
}

impl<T> std::ops::DerefMut for NodeIdVec<T> {
    fn deref_mut(&mut self) -> &mut Vec<T> {
        &mut self.0
    }
}

pub struct Node {
    parent: Option<NodeId>,
    children: Vec<NodeId>,
    id: NodeId,
}

impl Node {
    pub fn parent(&self) -> &Option<NodeId> {
        &self.parent
    }

    pub fn children(&self) -> &[NodeId] {
        &self.children[..]
    }

    pub fn id(&self) -> &NodeId {
        &self.id
    }
}

pub struct Tree {
    root: Option<NodeId>,
    nodes: NodeIdVec<Node>,
}

impl Tree {
    pub fn new() -> Tree {
        Tree {
            root: None,
            nodes: NodeIdVec(Vec::new()),
        }
    }

    pub fn root(&self) -> &Option<NodeId> {
        &self.root
    }

    pub fn create_root(&mut self) -> Option<NodeId> {
        if self.root.is_none() {
            self.root = Some(self.create_node());
            self.root
        } else {
            None
        }
    }

    pub fn nodes(&self) -> &[Node] {
        &self.nodes.0[..]
    }

    pub fn get_node(&self, node: NodeId) -> Option<&Node> {
        self.nodes.get(node)
    }

    pub fn get_mut_node(&mut self, node: NodeId) -> Option<&mut Node> {
        self.nodes.get_mut(node)
    }

    pub fn add_child(&mut self, node: NodeId) -> Option<NodeId> {
        match self.get_node(node) {
            Some(_) => {
                let id = self.create_node();
                self[node].children.push(id);
                self.set_parent(id, node);
                Some(id)
            }
            _ => None,
        }
    }

    fn set_parent(&mut self, node: NodeId, parent: NodeId) {
        self.get_mut_node(node).unwrap().parent = Some(parent);
    }

    fn create_node(&mut self) -> NodeId {
        let id = NodeId::new(self.nodes.len().try_into().unwrap());
        self.nodes.push(Node {
            parent: None,
            children: Vec::new(),
            id,
        });
        id
    }
}

impl Index<NodeId> for Tree {
    type Output = Node;

    fn index(&self, index: NodeId) -> &Self::Output {
        &self.nodes[index]
    }
}

impl IndexMut<NodeId> for Tree {
    fn index_mut(&mut self, index: NodeId) -> &mut Self::Output {
        &mut self.nodes[index]
    }
}

impl<'a> IntoIterator for &'a Tree {
    type Item = &'a Node;
    type IntoIter = Iter<'a, Node>;

    fn into_iter(self) -> Self::IntoIter {
        self.nodes.iter()
    }
}

impl<'a> IntoIterator for &'a mut Tree {
    type Item = &'a mut Node;
    type IntoIter = IterMut<'a, Node>;

    fn into_iter(self) -> Self::IntoIter {
        self.nodes.iter_mut()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::mem::size_of;
    #[test]
    fn node_id_size() {
        assert_eq!(size_of::<Option<NodeId>>(), size_of::<NodeId>());
    }
}
