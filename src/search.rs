use crate::graph::{NodeId, Tree};
use std::collections::VecDeque;

pub trait TreeIterator {
    fn next(&mut self, t: &Tree) -> Option<NodeId>;
}

pub struct IteratorAdapter<'a, T: TreeIterator> {
    pub searcher: T,
    pub tree: &'a Tree,
}

#[allow(dead_code)]
impl<'a, T: TreeIterator> IteratorAdapter<'a, T> {
    pub fn new(searcher: T, tree: &'a Tree) -> IteratorAdapter<'a, T> {
        IteratorAdapter { searcher, tree }
    }
}

impl<'a, T: TreeIterator> Iterator for IteratorAdapter<'a, T> {
    type Item = NodeId;
    fn next(&mut self) -> Option<Self::Item> {
        self.searcher.next(&self.tree)
    }
}

#[allow(dead_code)]
pub struct TreeDfs {
    stack: Vec<NodeId>,
    current_number: Option<usize>,
}

#[allow(dead_code)]
impl TreeDfs {
    pub fn new(start: NodeId, t: &Tree) -> Self {
        let mut tree_dfs = TreeDfs {
            stack: Vec::new(),
            current_number: None,
        };
        if t.get_node(start).is_some() {
            tree_dfs.restart(start);
        }
        tree_dfs
    }

    pub fn current_number(&self) -> Option<usize> {
        self.current_number
    }

    pub fn restart(&mut self, start: NodeId) {
        self.stack.clear();
        self.stack.push(start);
    }

    fn push_children(&mut self, node: NodeId, t: &Tree) {
        for child in t.get_node(node).unwrap().children().iter().rev() {
            self.stack.push(*child);
        }
    }
}

impl TreeIterator for TreeDfs {
    fn next(&mut self, t: &Tree) -> Option<NodeId> {
        match self.stack.pop() {
            Some(node) => {
                self.current_number = Some(self.current_number.map_or(0, |x| x + 1));
                self.push_children(node, t);
                Some(node)
            }
            None => None,
        }
    }
}

#[allow(dead_code)]
pub struct TreeBfs {
    queue: VecDeque<(NodeId, usize)>,
    current_depth: Option<usize>,
}

#[allow(dead_code)]
impl TreeBfs {
    pub fn new(start: NodeId, t: &Tree) -> Self {
        let mut tree_bfs = TreeBfs {
            queue: VecDeque::new(),
            current_depth: None,
        };
        if t.get_node(start).is_some() {
            tree_bfs.restart(start);
        }
        tree_bfs
    }

    pub fn current_depth(&self) -> Option<usize> {
        self.current_depth
    }

    pub fn restart(&mut self, start: NodeId) {
        self.queue.clear();
        self.queue.push_back((start, 0));
    }

    fn push_children(&mut self, node: NodeId, t: &Tree) {
        let child_depth = self.current_depth.map_or(0, |x| x + 1);
        for child in t.get_node(node).unwrap().children() {
            self.queue.push_back((*child, child_depth));
        }
    }
}

impl TreeIterator for TreeBfs {
    fn next(&mut self, t: &Tree) -> Option<NodeId> {
        match self.queue.pop_front() {
            Some((node, depth)) => {
                self.current_depth = Some(depth);
                self.push_children(node, t);
                Some(node)
            }
            None => None,
        }
    }
}

#[allow(dead_code)]
pub struct TreeHeightSearch {
    queue: VecDeque<(NodeId, usize)>,
    node_height: Vec<Option<usize>>,
    unvisited_children: Vec<usize>,
}

#[allow(dead_code)]
impl TreeHeightSearch {
    pub fn new(t: &Tree) -> Self {
        let queue: VecDeque<(NodeId, usize)> = t
            .nodes()
            .iter()
            .filter(|node| node.children().is_empty())
            .map(|node| (*node.id(), 0))
            .collect();
        let unvisited_children: Vec<usize> =
            t.nodes().iter().map(|node| node.children().len()).collect();
        TreeHeightSearch {
            queue,
            node_height: vec![None; t.nodes().len()],
            unvisited_children,
        }
    }

    pub fn next(&mut self, t: &Tree) -> Option<(NodeId, usize)> {
        match self.queue.pop_front() {
            Some((node, height)) => {
                self.update_parent(t, &node, height);
                Some((node, height))
            }
            None => None,
        }
    }

    fn update_parent(&mut self, t: &Tree, node: &NodeId, height: usize) {
        if let Some(parent) = t.get_node(*node).map(|node| *node.parent()).flatten() {
            self.node_height[parent.index()] =
                std::cmp::max(self.node_height[parent.index()], Some(height + 1));
            self.unvisited_children[parent.index()] -= 1;
            if self.unvisited_children[parent.index()] == 0 {
                self.queue
                    .push_back((parent, self.node_height[parent.index()].unwrap()));
            }
        }
    }
}

#[allow(dead_code)]
pub struct AncestorSearch {
    next: Option<NodeId>,
}

#[allow(dead_code)]
impl AncestorSearch {
    pub fn new(node_id: NodeId, t: &Tree) -> Self {
        match t.get_node(node_id) {
            Some(node) => AncestorSearch {
                next: *node.parent(),
            },
            None => AncestorSearch { next: None },
        }
    }
}

impl TreeIterator for AncestorSearch {
    fn next(&mut self, t: &Tree) -> Option<NodeId> {
        match self.next {
            Some(nodeid) => {
                self.next = t.get_node(nodeid).and_then(|node| *node.parent());
                Some(nodeid)
            }
            None => None,
        }
    }
}

enum DFSIndicator {
    Visit(NodeId),
    Withdraw(NodeId),
}

// Performs a dfs search of the graph but also contains revisits when the dfs withdraws to a node.
pub struct EulerTourSearch {
    stack: Vec<DFSIndicator>,
    withdraw: bool,
}

impl EulerTourSearch {
    pub fn new(node_id: NodeId, t: &Tree) -> Self {
        EulerTourSearch {
            stack: match t.get_node(node_id) {
                Some(_) => vec![DFSIndicator::Visit(node_id)],
                None => vec![],
            },
            withdraw: false,
        }
    }

    pub fn withdrawn(&self) -> bool {
        self.withdraw
    }

    fn push_children(&mut self, node: NodeId, t: &Tree) {
        for child in t.get_node(node).unwrap().children().iter().rev() {
            self.stack.push(DFSIndicator::Withdraw(node));
            self.stack.push(DFSIndicator::Visit(*child));
        }
    }

    pub fn next(&mut self, t: &Tree) -> Option<NodeId> {
        match self.stack.pop() {
            Some(DFSIndicator::Visit(node)) => {
                self.withdraw = false;
                self.push_children(node, t);
                Some(node)
            }
            Some(DFSIndicator::Withdraw(node)) => {
                self.withdraw = true;
                Some(node)
            }
            None => None,
        }
    }
}

impl TreeIterator for EulerTourSearch {
    fn next(&mut self, t: &Tree) -> Option<NodeId> {
        <EulerTourSearch>::next(self, t)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tree_generator::balanced_tree::generate_balanced_tree;
    use crate::tree_generator::random_tree::random_tree;
    #[test]
    fn basic_bfs_test() {
        let t = generate_balanced_tree(5, 5);
        let mut bfs = TreeBfs::new(t.root().unwrap(), &t);
        let mut count = 0;
        while let Some(_) = bfs.next(&t) {
            count += 1;
        }
        assert_eq!(count, t.nodes().len());
    }

    #[test]
    fn basic_dfs_test() {
        let t = generate_balanced_tree(5, 5);
        let mut dfs = TreeDfs::new(t.root().unwrap(), &t);
        let mut count = 0;
        while let Some(_) = dfs.next(&t) {
            count += 1;
        }
        assert_eq!(count, t.nodes().len());
    }

    #[test]
    fn basic_height_test() {
        let t = generate_balanced_tree(5, 5);
        let mut height = TreeHeightSearch::new(&t);
        let mut count = 0;
        while let Some(_) = height.next(&t) {
            count += 1;
        }

        assert_eq!(count, t.nodes().len());

        let tr = random_tree(500, [0; 32]);
        height = TreeHeightSearch::new(&tr);
        count = 0;
        while let Some(_) = height.next(&tr) {
            count += 1;
        }

        assert_eq!(count, tr.nodes().len());
    }
}
