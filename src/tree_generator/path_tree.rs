use crate::graph::{NodeIdVec, Tree};
use std::collections::VecDeque;

pub fn generate_path_tree(layers: usize, branching: usize, spacing: usize) -> Tree {
    let mut q = VecDeque::new();
    let mut t = Tree::new();
    let mut depth = NodeIdVec(Vec::new());
    depth.push(1);
    q.push_back(t.create_root().unwrap());
    while let Some(nodeid) = q.pop_front() {
        let node = &t[nodeid];
        let current_depth = depth[*node.id()];
        let current_layer = current_depth / spacing;
        if current_layer < layers {
            if current_depth % spacing == 0 {
                for _ in 0..branching {
                    q.push_back(t.add_child(nodeid).unwrap());
                    depth.push(current_depth + 1);
                }
            } else {
                q.push_back(t.add_child(nodeid).unwrap());
                depth.push(current_depth + 1);
            }
        }
    }
    t
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn current_number_of_nodes() {
        let t = generate_path_tree(1, 2, 4);
        assert_eq!(t.nodes().len(), 4);
        let t = generate_path_tree(2, 2, 4);
        assert_eq!(t.nodes().len(), 12);
        let t = generate_path_tree(3, 2, 4);
        assert_eq!(t.nodes().len(), 28);
    }
    //   * 1
    //   * 2
    //   * 3
    //   * 4
    //  / \
    //  * * 5
    //  * * 6
    //  * * 7
    //  * * 8
    // /\ /\
    //* * * * 9
    //* * * * 10
    //* * * * 11
    //* * * * 12
}
