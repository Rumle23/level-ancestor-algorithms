pub mod balanced_tree;
pub mod path_tree;
pub mod random_binary_tree;
pub mod random_tree;

use crate::graph::{NodeId, Tree};
use crate::level_ancestor::{
    LevelAncestor, LevelAncestorBuilder, MacroMicroLABuilder, Preprocessor,
};
use rand::{rngs::StdRng, seq::SliceRandom, Rng, SeedableRng};

pub fn generate_all_queries(t: &Tree) -> Vec<(NodeId, usize)> {
    let prep = Preprocessor::build(t);
    let mut queries = Vec::new();
    for node in t.nodes() {
        for i in 0..*prep.depth(node.id()).unwrap() {
            queries.push((*node.id(), i));
        }
    }

    queries
}

pub fn generate_queries_of_depth(t: &Tree, d: usize) -> Vec<(NodeId, usize)> {
    generate_all_queries(t)
        .into_iter()
        .filter(|(_n, query_depth)| query_depth == &d)
        .collect()
}

pub fn random_select_query(
    queries: &Vec<(NodeId, usize)>,
    number: usize,
    seed: [u8; 32],
) -> Vec<(NodeId, usize)> {
    let mut seeded_rng = StdRng::from_seed(seed);
    let mut result = Vec::new();
    result.reserve(number);
    for _ in 0..number {
        let random_query = *queries.choose(&mut seeded_rng).unwrap();
        result.push(random_query);
    }
    result
}

pub fn random_path_tree_query_generator(
    number_of_nodes: usize,
    number_of_queries: usize,
    seed: [u8; 32],
) -> Vec<(NodeId, usize)> {
    let mut seeded_rng = StdRng::from_seed(seed);
    let mut result = Vec::new();
    result.reserve(number_of_queries);
    for _ in 0..number_of_queries {
        let node = seeded_rng.gen_range(0, number_of_nodes);
        let depth = if node == 0 {
            0
        } else {
            seeded_rng.gen_range(0, node)
        };
        result.push((NodeId::new(node as u32), depth));
    }
    result
}
