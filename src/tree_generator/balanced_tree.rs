use crate::graph::{NodeIdVec, Tree};
use std::collections::VecDeque;

pub fn generate_balanced_tree(layers: usize, branching: usize) -> Tree {
    let mut q = VecDeque::new();
    let mut t = Tree::new();
    let mut depth = NodeIdVec(Vec::new());
    depth.push(0);
    q.push_back(t.create_root().unwrap());
    while let Some(nodeid) = q.pop_front() {
        let node = &t[nodeid];
        let current_layer = depth[*node.id()];
        if current_layer < layers {
            for _ in 0..branching {
                q.push_back(t.add_child(nodeid).unwrap());
                depth.push(current_layer + 1);
            }
        }
    }
    t
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn current_number_of_nodes() {
        let t = generate_balanced_tree(5, 2);
        assert_eq!(t.nodes().len(), 63);
    }
}
