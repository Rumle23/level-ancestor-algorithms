use crate::graph::{NodeId, Tree};
use rand::{rngs::StdRng, seq::SliceRandom, Rng, SeedableRng};

fn recursive_random_binary_tree(
    tree: &mut Tree,
    current_root: NodeId,
    number_of_nodes: usize,
    skew: f64,
    rng: &mut StdRng,
) {
    if number_of_nodes == 0 {
        return;
    }
    let left_subtree_size = if (number_of_nodes as f64 * skew) as usize != 0 {
        rng.gen_range(0, (number_of_nodes as f64 * skew) as usize)
    } else {
        0
    };
    let right_subtree_size = number_of_nodes - left_subtree_size;

    if left_subtree_size > 0 {
        let left_root = tree.add_child(current_root).unwrap();
        recursive_random_binary_tree(tree, left_root, left_subtree_size - 1, skew, rng);
    }
    if right_subtree_size > 0 {
        let right_root = tree.add_child(current_root).unwrap();
        recursive_random_binary_tree(tree, right_root, right_subtree_size - 1, skew, rng);
    }
}

pub fn random_binary_tree(number_of_nodes: usize, seed: [u8; 32], skew: f64) -> Tree {
    let mut seeded_rng = StdRng::from_seed(seed);
    let mut t = Tree::new();
    let root = t.create_root().unwrap();
    recursive_random_binary_tree(&mut t, root, number_of_nodes - 1, skew, &mut seeded_rng);
    t
}
