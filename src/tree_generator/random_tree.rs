use crate::graph::Tree;
use rand::{rngs::StdRng, seq::SliceRandom, SeedableRng};

pub fn random_tree(number_of_nodes: usize, seed: [u8; 32]) -> Tree {
    let mut seeded_rng = StdRng::from_seed(seed);
    let mut t = Tree::new();
    let mut nodes = vec![t.create_root().unwrap()];
    for _ in 1..number_of_nodes {
        let parent = *nodes.choose(&mut seeded_rng).unwrap();
        nodes.push(t.add_child(parent).unwrap());
    }
    t
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_random() {
        let tree1 = random_tree(10, [0; 32]);
        let tree2 = random_tree(10, [1; 32]);
        let mut different = false;
        for node in tree1.nodes() {
            different |=
                node.children().len() != tree2.get_node(*node.id()).unwrap().children().len();
        }

        assert_eq!(tree1.nodes().len(), 10);
        assert_eq!(tree2.nodes().len(), 10);

        assert!(different);
        different = false;
        let tree3 = random_tree(10, [0; 32]);

        for node in tree1.nodes() {
            different |=
                node.children().len() != tree3.get_node(*node.id()).unwrap().children().len();
        }
        assert!(!different);

        let tree4 = random_tree(11, [0; 32]);

        let mut num_different = 0;

        for node in tree1.nodes() {
            if node.children().len() != tree4.get_node(*node.id()).unwrap().children().len() {
                num_different += 1;
            }
        }

        assert_eq!(num_different, 1);
    }
}
